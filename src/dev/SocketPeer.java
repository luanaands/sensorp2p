package dev;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Scanner;


public class SocketPeer {

	
	public static void main(String[] args) throws IOException {
		
	    Scanner scanner = new Scanner(System.in);
	    System.out.println("Source Port:");
		int sourcePort = Integer.parseInt(scanner.nextLine());
		
		System.out.println("Destination Ip:");
		String destinationIp = scanner.nextLine();
		
		System.out.println("Destination Port:");
		int DestinationPort = Integer.parseInt(scanner.nextLine());
		scanner.close();
		
        Channel channel = new Channel();
        channel.bind(sourcePort);
        channel.start();
		System.out.print("Started");
		
		Sensor sensor = new Sensor();
		InetSocketAddress address = new InetSocketAddress(destinationIp, DestinationPort);
		
		while(true){
		   	
			String value = sensor.getName() + ">>> " + sensor.Randomize();
			
			String[] number = value.split(" ");
		
			if(Integer.parseInt(number[1]) == 0 && value.isEmpty()){
				break;
			}
			
			channel.Deliver(address, value);
			System.out.println(value);
		}
		
		channel.stop();
		System.out.print("Closed");
		
	}


}
