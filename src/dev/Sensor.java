package dev;

import java.util.Random;

public class Sensor {

	private String name;
	private Random random;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Random getRandom() {
		return random;
	}
	
	// set a name arbitrary
	public Sensor() {
		// set a name arbitrary
		this.random = new Random();
		String tag = String.valueOf(getRandom().nextInt(10));
		this.setName("Sensor-" + tag);
		
	}
	
	// get a value arbitrary
	public String Randomize() {
		return String.valueOf(getRandom().nextInt());
	}


	
}
