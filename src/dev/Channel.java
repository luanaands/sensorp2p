package dev;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;


public class Channel implements Runnable {

	private DatagramSocket socket;
	private boolean running;
	
	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}


	public void bind(int port) throws SocketException {
		socket = new DatagramSocket(port);
	}
    
	// start the socket with a thread 
	public void start() {

		Thread thread = new Thread(this);
		thread.start();
	}

	// close the socket 
	public void stop() {

		setRunning(false);
		socket.close();
		System.out.println("Closed");
	}

	// run of the thread 
	@Override
	public void run() {
	
		byte[] buffer = new byte[50];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
	
	    
		setRunning(true);
		while (isRunning()) {
			try {
				
				//System.out.println(message);
			
				socket.receive(packet);
				
				
				String message = new String( buffer, 0, packet.getLength());
				System.out.println(message);
				
			} catch (IOException e) {				
				break;
			}
			
		}

	}


	// send the message to address
	public void Deliver(InetSocketAddress address, String msg) throws IOException {

		byte[] buffer = msg.getBytes();
		int va = buffer.length;
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		packet.setSocketAddress(address);
		socket.send(packet);
	/*	writer.println(msg);
		writer.flush();*/
		
	}

}
